import time
from random import randint
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap


#grafy vyvoje obyvatel
def graf_span():
    x = [2013, 2014, 2015, 2016, 2017]
    y = [46.62, 46.48, 46.44, 46.48, 46.57]
    plt.plot(x, y, color='green', linestyle='dashed', linewidth=3,
             marker='o', markerfacecolor='blue', markersize=12)
    plt.ylim(45, 48)
    plt.xlim(2013, 2017)
    plt.xlabel('rok')
    plt.ylabel('počet obyvatel v milionech')
    plt.title('2. indicie')
    plt.show()


def graf_fran():
    x = [2013, 2014, 2015, 2016, 2017]
    y = [66.00, 66.32, 66.59, 66.86, 67.12]
    plt.plot(x, y, color='green', linestyle='dashed', linewidth=3,
             marker='o', markerfacecolor='blue', markersize=12)
    plt.ylim(65, 68)
    plt.xlim(2013, 2017)
    plt.xlabel('rok')
    plt.ylabel('počet obyvatel v milionech')
    plt.title('2. indicie')
    plt.show()


def graf_brit():
    x = [2013, 2014, 2015, 2016, 2017]
    y = [64.13, 64.61, 65.13, 65.60, 66.02]
    plt.plot(x, y, color='green', linestyle='dashed', linewidth=3,
             marker='o', markerfacecolor='blue', markersize=12)
    plt.ylim(63, 66)
    plt.xlim(2013, 2017)
    plt.xlabel('rok')
    plt.ylabel('počet obyvatel v milionech')
    plt.title('2. indicie')
    plt.show()


#vysledna mapa statu
def map_span():
    map = Basemap(llcrnrlon=-18.5, llcrnrlat=31, urcrnrlon=17., urcrnrlat=52.,
                  resolution='i', projection='tmerc', lat_0=39.5, lon_0=-3.25)
    map.drawmapboundary(fill_color='aqua')
    map.fillcontinents(color='grey', lake_color='aqua')
    map.drawcoastlines()
    plt.show()


def map_fran():
    map = Basemap(llcrnrlon=-14.5, llcrnrlat=34, urcrnrlon=29., urcrnrlat=56.,
                  resolution='i', projection='tmerc', lat_0=39.5, lon_0=-3.25)
    map.drawmapboundary(fill_color='aqua')
    map.fillcontinents(color='grey', lake_color='aqua')
    map.drawcoastlines()
    plt.show()


def map_brit():
    map = Basemap(llcrnrlon=-21.5, llcrnrlat=39, urcrnrlon=19., urcrnrlat=61.,
                  resolution='i', projection='tmerc', lat_0=39.5, lon_0=-3.25)
    map.drawmapboundary(fill_color='aqua')
    map.fillcontinents(color='grey', lake_color='aqua')
    map.drawcoastlines()
    plt.show()


#nabozenstvi
def nab_span():
    activities = ['křesťanství', 'jiné náboženství', 'bez vyznání']
    slices = [68, 2, 27]
    colors = ['r', 'y', 'g']
    plt.pie(slices, labels=activities, colors=colors,
            startangle=90, shadow=True, explode=(0, 0, 0),
            radius=1.2, autopct='%1.1f%%')
    plt.legend()
    plt.show()


def nab_fran():
    activities = ['křesťanství', 'islám', 'protestanství', 'judaismus']
    slices = [83, 5, 1, 1]
    colors = ['r', 'y', 'g', 'y']
    plt.pie(slices, labels=activities, colors=colors,
            startangle=90, shadow=True, explode=(0, 0, 0, 0),
            radius=1.2, autopct='%1.1f%%')
    plt.legend()
    plt.show()


def nab_brit():
    activities = ['křesťanství', 'islám', 'hinduismus', 'bez vyznání']
    slices = [72, 3, 1, 20]
    colors = ['r', 'y', 'g', 'y']
    plt.pie(slices, labels=activities, colors=colors,
            startangle=90, shadow=True, explode=(0, 0, 0, 0),
            radius=1.2, autopct='%1.1f%%')
    plt.legend()
    plt.show()


def hra():
    konec_hry = True
    while konec_hry == True:
        typ = randint(1,3)
        print("GEOGRAFICKÁ HRA\nCílem hry je dle indicií poznat stát světa")
        time.sleep(3)
        print("První indicií je tato mapa, hledaný stát se nachází v ní :-)")
        if typ == 1:
            print(map_span())
        elif typ == 2:
            print(map_fran())
        elif typ == 3:
            print(map_brit())
        time.sleep(3)
        print("Další nápovědou je graf vývoje počtu obyvatel v hledaném státu")
        if typ == 1:
            print(graf_span())
        elif typ == 2:
            print(graf_fran())
        elif typ == 3:
            print(graf_brit())
        time.sleep(3)
        print("Třetí nápovědou je graf zastoupení náboženství v daném státu")
        if typ == 1:
            print(nab_span())
        elif typ == 2:
            print(nab_fran())
        elif typ == 3:
            print(nab_brit())
        print("Poslední nápovědou je, že hledaný stát je jedním z těchto:")
        time.sleep(2)
        print("POLSKO\nVELKÁ BRITÁNIE\nIRSKO\nFRANCIE\nPORTUGALSKO\nŠPANĚLSKO\nNĚMECKO\nITÁLIE")
        time.sleep(2)
        zk = input("Napiš o jaký stát se podle tebe jedná???")
        if typ == 1 and zk == "ŠPANĚLSKO" or "Španělsko" or "španělsko":
            print("Gratuluji, vyhrál jsi")
        elif typ == 2 and zk == "FRANCIE" or "Francie" or "francie":
            print("Gratuluji, vyhrál jsi")
        elif typ == 3 and zk == "VELKÁ BRITÁNIE" or "Velká Británie" or "velká británie":
            print("Gratuluji, vyhrál jsi")
        else:
            print("bohužel, tvoje odpověď není správná")
        konec = input("chceš si zahrát novou hru??[a/n]")
        if konec == "a" or "A":
            konec_hry = True
        else:
            konec_hry = False


print(hra())