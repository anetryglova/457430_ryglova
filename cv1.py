print("Hello")


x = 3.14
if type(x) == float:
    print("x je reálné číslo")

print("ahoj" * 300)

from turtle import forward

forward(50)

from turtle import left, right

forward(50)
left(60)
forward(50)
right(60)
forward(50)

from turtle import forward, left, exitonclick

forward(50)
left(90)
forward(50)
left(90)
forward(50)
left(90)
forward(50)
left(90)

from turtle import forward, left, exitonclick

forward(100)
left(90)
forward(50)
left(90)
forward(100)
left(90)
forward(50)
left(90)



for cislo in range(5):
    print(cislo)

for pozdrav in 'Ahoj', 'Hello', 'Hola', 'Hei', 'SYN':
    print(pozdrav + '!')


from turtle import forward, left, exitonclick

for i in range(4):
    forward(50)
    left(90)

exitonclick()