import random
import time

while True:
    lehka_varianta = ("zub", "kmen", "plod", "pes", "les")
    stredni_varianta = ("lampa", "konvice", "stojan", "lokomotiva", "nanuk")
    tezka_varianta = ("encyklopedie", "rodedendron", "krkolomnost", "kytička", "stalaktit", "dalmatin")
    vyber_hry = input("Jakou obtížnost hry Šibenice chceš hrát? [lehka/stredni/tezka]\nZadej:")

    if vyber_hry == "lehka":
        vybrane_slovo = random.choice(lehka_varianta).upper()
    elif vyber_hry == "stredni":
        vybrane_slovo = random.choice(stredni_varianta).upper()
    else:
        vybrane_slovo = random.choice(tezka_varianta).upper()

    spatne = len(vybrane_slovo) - 1
    znak = (" ? ") * len(vybrane_slovo)
    pouzita = []
    chybne_pokusy = 0
    uhodnuta_pismena = []
    print()
    print("Vítej ve hře")
    print()
    time.sleep(1)


    while chybne_pokusy < spatne and znak != vybrane_slovo:
        hotovo = True
        print("Použitá písmena: ", pouzita)
        pismeno = input("Zadej písmeno: ").upper()
        print()

        while pismeno in pouzita:
            print("Tohle písmeno už jsi použil")
            pismeno = input("Zadej písmeno: ").upper()
            print()
        pouzita.append(pismeno)

        if pismeno in vybrane_slovo:
            print()

            uhodnuta_pismena.append(pismeno)

        else:
            print("Zkus to znovu!")
            chybne_pokusy += 1

        for i in range(len(vybrane_slovo)):
            if  vybrane_slovo[i] in uhodnuta_pismena:
                for j in range(len(uhodnuta_pismena)):
                    if uhodnuta_pismena[j] == vybrane_slovo[i]:
                        print(uhodnuta_pismena[j], end = " ")
            else:
                print("?", end = " ")
                hotovo = False

        if hotovo:
            print()
            print("Vyhrál jsi!\nGratuluji!!\n ")
            break

    if chybne_pokusy == spatne:
        print("Prohrál jsi!")

    a = input("Nová hra? [ano/ne]\n ")
    if a == "ne" or a == "Ne":
        break











