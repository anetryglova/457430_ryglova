# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'

string1_1 = string1.split()
print(string1_1[0] + " " + string1_1[1] + " " + string1_1[2] + "...")


# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.

print(string1[(len(string1)-11):])

# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'

string2_2 = string2.split(", ")
for i in range(len(string2_2)):
    print(string2_2[i])


# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.

def freq_analysis_letters(text):
    ls = []
    dc = {}
    text = text.lower()
    for letter in text:
        if letter.isalpha():
            dc[letter] = text.count(letter)
    for key in sorted(dc):
        print(key,":", dc[key])

freq_analysis_letters(string2)

# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved: Počet sekund, které uplynuly od 1. ledna 1970 bez započítání přestupných sekund
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
datetime1 = '20181121191930'
import datetime

import datetime
result = datetime.datetime(year=int(datetime1[0:4]),
                                month=int(datetime1[4:6]),
                                day=int(datetime1[6:8]),
                                hour=int(datetime1[8:10]),
                                minute=int(datetime1[10:12]),
                                second=int(datetime1[12:14]))
print(result)
today = datetime.datetime.today()
print(today)
print(today - result)


# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'

string3 = string3.split()
correct_string3 = correct_string3.split()

str3 = [" ", " ", " ", " "," ", " "," ", " "," "]
st = ""
for i in range(len(string3)):
    for j in range(len(correct_string3)):
        if correct_string3[j] == string3[i]:
            print(correct_string3[j].lower(),":",  abs(i-j))
            str3[j] = string3[i]
for k in range(len(str3)):
    st = st + str3[k] + " "
print(st)







